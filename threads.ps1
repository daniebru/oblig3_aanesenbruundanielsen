Clear-Host
$nettleser = "firefox" #Bruker firefox istedenfor chrome

$array = (get-process | Where-Object {$_.ProcessName -match $nettleser}).Id #finner alle id for prosesser med navn firefox

Write-Output $("  Pnavn    PID   Tråder")

foreach ( $i in $array ) { #Går igjennom alle pid i array
  $antall = ((Get-Process -id $i).Threads).count #Leser inn antall tråder for en prosess i antall
  Write-Output $("{0} {1,6}   {2,3}" -f $nettleser,$i,$antall)
}

Write-Output ""
