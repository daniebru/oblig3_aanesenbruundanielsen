do {

  Write-Output "
      1 - Hvem er jeg og hva er navnet på dette scriptet?
      2 - Hvor lenge er det siden siste boot?
      3 - Hvor mange prosesser og tråder finnes?
      4 - Hvor mange context switcher fant sted siste sekund?
      5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?
      6 - Hvor mange interrupts fant sted siste sekund?
      9 - Avslutt dette scriptet";

  switch ( $tall = Read-Host -Prompt "`nSkriv inn tall" ) {
      1 {
          Clear-Host
          $navn = $MyInvocation.MyCommand.Name   #Finner info om skript og henter ut navn fra .Name
          Write-Output $("Bruker: $env:USERNAME. Skriptnavn: $navn"); #evn: finner navn fra environment variabel
        }

      2 {
        Clear-Host
        Write-Output $($objekt = Get-CimInstance win32_operatingsystem;
          $oppe = (Get-Date) - $($objekt.LastBootUpTime)   #Dagens dato fra Get-Date minus LastBootUpTime
          ("Siste boot: " + $($objekt.LastBootUpTime))     #Skriver dato og tid for siste boot
          ("`nOppetid: " + $oppe.Days + " Dager " + $oppe.Hours + " Hours " + $oppe.Minutes + " Minutes")) #Regner ut tid siden siste boot
        }

      3 {
        Clear-Host
        Write-Output $("Antall prosesser:"; $((Get-Process).count));   #Finner antall prosesser med .count
        Write-Output "Antall Tråder: "; $((Get-CimInstance win32_Thread).count); #Antall tråder fra ciminstance med .count for antall
      }

      4 {
        Clear-Host
        Write-Output $("Context switcher siste sekund:"; $((Get-CimInstance Win32_PerfFormattedData_PerfOS_System).ContextSwitchesPersec));
      }

      5 {
        Clear-Host
        Write-Output $("Andel CPU-tid benyttet siste sekund:")
        Write-Output $("Kernel mode: " ; $((Get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation).PercentPrivilegedTime); )
        Write-Output $("User mode: " ; $((Get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation).PercentUserTime); )
      }

      6 {
        Clear-Host
        Write-Output $("Interrupts per sekund: "; $((Get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation).InterruptsPersec); )
      }

      9 {
        Clear-Host
      }

      Default {
        Clear-Host
        Write-Output $("####   Ukjent kommando") #Space for å få med hele bakgrunnsfargen
      }
  }

  Write-Output " ";
} while( $tall -ne 9 )
