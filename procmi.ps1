$array = $args #Kopierer over argumentlista fra kommandolinje

ForEach ( $i in $array ) {  #Går igjennom alle PIDer fra argumentliste
  $prosess = "$i"  #Gjør lagret $i verdi til streng for bruk i $fil og $tekst

  $fil = $prosess + "-" + $(Get-Date -format yyyyMMdd) + "-" + $(Get-Date -Format HHmmss) + ".meminfo" #Filnavn med get-date og file extension

  $pname = (Get-Process -Id $prosess).ProcessName #Henter ut prosessnavn fra Get-Process

  $vb = (Get-Process -Id $i).VB
  $ws = (Get-Process -Id $i).WS
  #$vb = (Get-Counter -counter "\Process($pname)\Virtual Bytes").countersamples.cookedvalue #Bruker prosessnavn for å hente ut verdi i byte
  #$ws = (Get-Counter -counter "\Process($pname)\Working Set").countersamples.cookedvalue

  $virtualc = [math]::Round($vb / 1MB, 2)  #Konverterer bytes til MB med math::round. (, 2) for 2 desimaler
  $workingc = [math]::Round($ws / 1MB, 2)

  $virtual = "$virtualc" + "MB"  #Gjør om convert-variablene til string for å legge til benevning MB, KB
  $working = "$workingc" + "MB"

  $tekst="******** INFO PROSESS ID $prosess ********
  Total bruk av virtual memory: $virtual
  Størrelse på Working Set: $working"


  ($tekst) > $fil #Lager fil med riktig innhold og filnavn
}
